<?php
// $Id$
/**
 * @file
 *   privatemsg_new task plugin.
 */

/**
 * Specialized implementation of hook_page_manager_task_tasks(). See api-task.html for
 * more information.
 */
function privatemsg_panels_privatemsg_new_page_manager_tasks() {
  if (!module_exists('privatemsg')) {
    return;
  }

  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',

    'title' => t('Privatemsg write'),
    'admin title' => t('Privatemsg write'),
    'admin description' => t('TODO'),
    'admin path' => variable_get('privatemsg_url_prefix', 'messages')  . '/new',

    // Menu hooks so that we can alter the node/%node menu entry to point to us.
    'hook menu alter' => 'privatemsg_panels_privatemsg_new_site_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('privatemsg_panels_privatemsg_new_disabled', TRUE),
    'enable callback' => 'privatemsg_panels_privatemsg_new_site_enable',
  );
}

/**
 * Callback defined by page_manager_contact_site_page_manager_tasks().
 *
 * Alter the node edit input so that node edit comes to us rather than the
 * normal node edit process.
 */
function privatemsg_panels_privatemsg_new_site_menu_alter(&$items, $task) {
  if (variable_get('privatemsg_panels_privatemsg_new_disabled', TRUE)) {
    return;
  }

  $url_prefix = variable_get('privatemsg_url_prefix', 'messages');
  $callback = $items[$url_prefix . '/new']['page callback'];
  $callback_arguments = $items[$url_prefix . '/new']['page arguments'];
  // Override the node edit handler for our purpose.
  if (($callback == 'drupal_get_form'  && $callback_arguments = array('privatemsg_privatemsg_new')) || variable_get('page_manager_override_anyway', FALSE)) {
    $items[$url_prefix . '/new']['page callback'] = 'privatemsg_panels_privatemsg_new_site';
    $items[$url_prefix . '/new']['file path'] = $task['path'];
    $items[$url_prefix . '/new']['file'] = $task['file'];
  }
  else {
    variable_set('privatemsg_panels_privatemsg_new_disabled', TRUE);
    if (!empty($GLOBALS['privatemsg_panels_privatemsg_new_disabled'])) {
      drupal_set_message(t('Page manager module is unable to enable privatemsg_new because some other module already has overridden with %callback.', array('%callback' => $callback)), 'warning');
    }
    return;
  }
}

/**
 * Callback to enable/disable the page from the UI.
 */
function privatemsg_panels_privatemsg_new_site_enable($cache, $status) {
  variable_set('privatemsg_panels_privatemsg_new_disabled', $status);
  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['privatemsg_panels_privatemsg_new_disabled'] = TRUE;
  }
}

function privatemsg_panels_privatemsg_new_site() {
  // Load my task plugin
  $task = page_manager_get_task('privatemsg_new');

  ctools_include('context');
  ctools_include('context-task-handler');
  $output = ctools_context_handler_render($task, '', array(), array());
  if ($output !== FALSE) {
    return $output;
  }

  $function = 'drupal_get_form';
  foreach (module_implements('page_manager_override') as $module) {
    $call = $module . '_page_manager_override';
    if (($rc = $call('privatemsg_new')) && function_exists($rc)) {
      $function = $rc;
      return $function();
      break;
    }
  }

  $vars = privatemsg_panels_url_prefix_helper();
  $url_prefix_arg_count = $vars['url_prefix_arg_count'];

  $recipient = arg($url_prefix_arg_count + 1);
  $subject = arg($url_prefix_arg_count + 2);
  $block->content = drupal_get_form('privatemsg_new', $recipient, $subject);
}


