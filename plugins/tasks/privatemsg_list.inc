<?php
// $Id$
/**
 * @file
 *   privatemsg_list task plugin.
 */

function privatemsg_panels_privatemsg_list_page_manager_tasks() {
  if (!module_exists('privatemsg')) {
    return;
  }

  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',

    'title' => t('Privatemsg list (All message)'),
    'admin title' => t('Privatemsg list (All messages)'),
    'admin description' => t('TODO'),
    'admin path' => variable_get('privatemsg_url_prefix', 'messages')  . '/list',

    // Menu hooks so that we can alter the node/%node menu entry to point to us.
    'hook menu alter' => 'privatemsg_panels_privatemsg_list_site_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('privatemsg_panels_privatemsg_list_disabled', TRUE),
    'enable callback' => 'privatemsg_panels_privatemsg_list_site_enable',
  );
}

/**
 * Callback defined by page_manager_contact_site_page_manager_tasks().
 *
 * Alter the node edit input so that node edit comes to us rather than the
 * normal node edit process.
 */
function privatemsg_panels_privatemsg_list_site_menu_alter(&$items, $task) {
  if (variable_get('privatemsg_panels_privatemsg_list_disabled', TRUE)) {
    return;
  }

  $url_prefix = variable_get('privatemsg_url_prefix', 'messages');

  $path = $url_prefix . '/list';
  $callback = $items[$path]['page callback'];
  $callback_arguments = $items[$path]['page arguments'];
  // Override the node edit handler for our purpose.
  if ($callback == 'privatemsg_list_page'  || variable_get('page_manager_override_anyway', FALSE)) {
    $items[$url_prefix]['page callback'] = $items[$path]['page callback'] = 'privatemsg_panels_privatemsg_list_site';
    $items[$url_prefix]['file path'] = $items[$path]['file path'] = $task['path'];
    $items[$url_prefix]['file'] = $items[$path]['file'] = $task['file'];
  }
  else {
    variable_set('privatemsg_panels_privatemsg_list_disabled', TRUE);
    if (!empty($GLOBALS['privatemsg_panels_privatemsg_list_disabled'])) {
      drupal_set_message(t('Page manager module is unable to enable privatemsg_list because some other module already has overridden with %callback.', array('%callback' => $callback)), 'warning');
    }
    return;
  }
}

/**
 * Callback to enable/disable the page from the UI.
 */
function privatemsg_panels_privatemsg_list_site_enable($cache, $status) {
  variable_set('privatemsg_panels_privatemsg_list_disabled', $status);
  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['privatemsg_panels_privatemsg_list_disabled'] = TRUE;
  }
}

function privatemsg_panels_privatemsg_list_site() {
  // Load my task plugin
  $task = page_manager_get_task('privatemsg_list');

  ctools_include('context');
  ctools_include('context-task-handler');
  $output = ctools_context_handler_render($task, '', array(), array());
  if ($output !== FALSE) {
    return $output;
  }

  $function = 'drupal_get_form';
  foreach (module_implements('page_manager_override') as $module) {
    $call = $module . '_page_manager_override';
    if (($rc = $call('privatemsg_list')) && function_exists($rc)) {
      $function = $rc;
      return $function();
      break;
    }
  }

  module_load_include('inc', 'privatemsg', 'privatemsg.pages');
  $vars = privatemsg_panels_url_prefix_helper();
  $url_prefix_user_arg_position = $vars['url_prefix_user_arg_position'];

  return privatemsg_list_page('list', arg($url_prefix_user_arg_position));

}


