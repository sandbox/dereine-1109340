<?php
// $Id$
/**
 * @file
 *   TODO
 */


/**
 * Implementation of hook_ctools_content_types().
 */
function privatemsg_panels_private_actionlinks_ctools_content_types() {
  if (!module_exists('privatemsg')) {
    return;
  }

  return array(
    'title' => t('Privatemsg Actions'),
    'icon' => 'icon_user.png',
    'single' => FALSE,
    'description' => t('Privatemsg TODO'),
    //'required context' => new ctools_context_required(t('User'), 'user'),
    'category' => t('Privatemsg'),
    //'defaults' => array('image_path' => '', 'template_file' => 'author-pane'),
  );
}

// The function name is <code>MODULE_NAME_CT_NAME_content_type_render
function privatemsg_panels_privatemsg_actionlinks_content_type_render($subtype, $conf, $panel_args, $context) {
}

