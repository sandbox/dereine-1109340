<?php
// $Id$
/**
 * @file
 *   TODO
 */

if (!module_exists('privatemsg')) {
  return;
}

$plugin = array(
  'title' => t('Privatemsg: messages(all/inbox/sent)'),
  'icon' => 'icon_user.png',
  'single' => TRUE,
  'description' => t('Privatemsg TODO'),
  'category' => t('Privatemsg'),
);

function privatemsg_panels_privatemsg_list_content_type_render($subtype, $conf, $panel_args, $context) {
  if (privatemsg_user_access()) {
    module_load_include('inc', 'privatemsg', 'privatemsg.pages');
    $vars = privatemsg_panels_url_prefix_helper();
    $url_prefix_user_arg_position = $vars['url_prefix_user_arg_position'];

    $block = new stdClass();

    $block->title = privatemsg_title_callback('Messages');
    $block->content = privatemsg_list_page($conf['list_type'], arg($url_prefix_user_arg_position));
    return $block;
  }
}

function privatemsg_panels_privatemsg_list_content_type_edit_form(&$form, &$form_state) {
  $conf = $form_state['conf'];

  $options['list'] = t('All messages');
  if (module_exists('privatemsg_filter')) {
    $options['sent'] = t('Send messages');
    $options['inbox'] = t('Inbox');
  }
  $form['list_type'] = array(
    '#title' => t('List type'),
    '#type' => 'select',
    '#options' => $options,
    '#description' => t('Choose which list type should be displayed'),
    '#default_value' => $conf['list_type'],
  );
}

/**
 * Submit handler for contact form.
 */
function privatemsg_panels_privatemsg_list_content_type_edit_form_submit(&$form, &$form_state) {
  $form_state['conf']['list_type'] = $form_state['values']['list_type'];
}
