<?php
// $Id$
/**
 * @file
 *   test
 */

if (!module_exists('privatemsg')) {
  return;
}

$plugin = array(
  'title' => t('Privatemsg Listing'),
  'icon' => 'icon_user.png',
  'single' => FALSE,
  'description' => t('Privatemsg TODO'),
  //'required context' => new ctools_context_required(t('User'), 'user'),
  'category' => t('Privatemsg'),
  //'defaults' => array('image_path' => '', 'template_file' => 'author-pane'),
);

// The function name is <code>MODULE_NAME_CT_NAME_content_type_render
function privatemsg_panels_privatemsg_content_type_render($subtype, $conf, $panel_args, $context) {
  if (!privatemsg_user_access()) {
    return;
  }

  $block = new StdClass();
  $block->module = 'privatemsg';
  $block->delta = 'list';
  $block->title = privatemsg_title_callback('Messages');

}

