<?php
// $Id$
/**
 * @file
 *   TODO
 */

if (!module_exists('privatemsg')) {
  return;
}

$plugin = array(
  'title' => t('Privatemsg: Write new message'),
  'icon' => 'icon_user.png',
  'single' => TRUE,
  'description' => t('Privatemsg TODO'),
  'category' => t('Privatemsg'),
);

function privatemsg_panels_privatemsg_new_content_type_render($subtype, $conf, $panel_args, $context) {
  if (user_access('write privatemsg')) {
    module_load_include('inc', 'privatemsg', 'privatemsg.pages');
    $block = new StdClass();
    $vars = privatemsg_panels_url_prefix_helper();
    $url_prefix_arg_count = $vars['url_prefix_arg_count'];

    $recipient = arg($url_prefix_arg_count + 1);
    $subject = arg($url_prefix_arg_count + 2);
    $block->content = drupal_get_form('privatemsg_new', $recipient, $subject);
    return $block;
  }
}

